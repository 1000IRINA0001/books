package books;

public class Book {
    private String author;
    private String bookTitle;
    private int publishingYear;
    private Genre genre;
    private String url;

    public Book(String author, String bookTitle, int publishingYear, Genre genre, String url) {
        this.author = author;
        this.bookTitle = bookTitle;
        this.publishingYear = publishingYear;
        this.genre = genre;
        this.url = url;
    }

    public String getAuthor() {
        return author;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public int getPublishingYear() {
        return publishingYear;
    }

    public Genre getGenre() {
        return genre;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String toString() {
        return "Book{" +
                "author='" + author + '\'' +
                ", bookTitle='" + bookTitle + '\'' +
                ", publishingYear=" + publishingYear +
                ", genre=" + genre +
                ", url='" + url + '\'' +
                '}';
    }
}