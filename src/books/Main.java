package books;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        Book book1 = new Book("Пушкин", "Капитанская дочка", 2000, Genre.NOVEL, "D:\\Captain_daughter");
        Book book2 = new Book("Носов", "Живая шляпа", 1996, Genre.TALE, "D:\\Live_hat");
        Book book3 = new Book("Лермонтов", "Цыганы", 2007, Genre.DRAMA, "D:\\Gypsies");
        Book book4 = new Book("Пушкин", "Борис Годунов", 2001, Genre.DRAMA, "D:\\Boris_Godunov");
        Book book5 = new Book("Бхаргава", "Грокаем алгоритмы", 2015, Genre.SCIENTIFIC, "D:\\Grok_Algorithms");
        Book book6 = new Book("Грибоедов", "Горе от ума", 2008, Genre.COMEDY, "D:\\Woe_from_wit");
        Book book7 = new Book("Чехов", "Медведь", 1985, Genre.COMEDY, "D:\\Bear");
        Book book8 = new Book("Толстой", "Война и мир", 2004, Genre.NOVEL, "D:\\War_add_Peace");
        Book book9 = new Book("Булгаков", "Мастер и Маргарита", 2000, Genre.NOVEL, "D:\\Master_and_Margarita");
        Book book10 = new Book("Фонвизин", "Недоросль", 1985, Genre.COMEDY, "D:\\Undergrowth");

        ArrayList<Book> genreBook = new ArrayList<>();
        ArrayList<Book> persons = new ArrayList<>();
        ArrayList<Book> names = new ArrayList<>();
        /**
         * создан и заполнен ArrayLIst с книками
         */
        ArrayList<Book> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);
        books.add(book3);
        books.add(book4);
        books.add(book5);
        books.add(book6);
        books.add(book7);
        books.add(book8);
        books.add(book9);
        books.add(book10);
        // String messageForSort = "не введено данных о книге";
//        String messageForChecksName = "Книг с таким названием не нашлось";
//        String messageForFiltrationGenre = "Книг этого жанра не нашлось";
//        String messageForSearchName = "Книг этого автора не нашлось";

        System.out.println("Сортировка книг по авторам в алфавитном порядке: ");
        print(SortAuthor(books));

        System.out.println("Сортировка книг по году издания: ");
        print(SortYear(books));

        System.out.println("Сортировка книг по названию");
        print(bookSorting(books));

        System.out.println("Введите название книги");
        String nameBook = scanner.nextLine();
       // isEmpty(checksName(books, names, nameBook), messageForChecksName);
        print(checksName(books, names, nameBook));

        System.out.println("Введите автора: ");
        String surname = scanner.nextLine();
        //isEmpty(SearchName(books, persons, surname), messageForSearchName);
        print(SearchName(books, persons, surname));

        System.out.println("Какой жанр литературы вы ищете");
        String genr = scanner.next();
        //isEmpty(filtrationGenre(books, genreBook, genr), messageForFiltrationGenre);
        print(filtrationGenre(books, genreBook, genr));


    }

    /**
     * метод сортирует книги по авторам с помощью сортировки выбором
     *
     * @param books массив книг
     * @return возращает отсортированный массив книг
     */
    private static ArrayList<Book> SortAuthor(ArrayList<Book> books) {
        Book temp;
        for (int i = 0; i < books.size(); i++) {
            for (int j = i + 1; j < books.size(); j++) {
                int compare = books.get(j).getAuthor().compareTo(books.get(i).getAuthor());
                if (compare < 0) {
                    temp = books.get(i);
                    books.set(i, books.get(j));
                    books.set(j, temp);
                }
            }

        }
        return books;
    }

    /**
     * метод сортирует сортирует книги по году издания(сначала новинки)
     * @param books массив книг
     * @return возвращает отсортированный по годам массив книг
     */
    private static ArrayList<Book> SortYear(ArrayList<Book> books) {
        Book temp;
        for (int i = 0; i < books.size(); i++) {
            for (int j = i + 1; j < books.size(); j++) {
                if (books.get(j).getPublishingYear() > books.get(i).getPublishingYear()) {
                    temp = books.get(i);
                    books.set(i, books.get(j));
                    books.set(j, temp);
                }
            }

        }
        return books;
    }

    /**
     * метод сортирует книги по их названию (в алфавитном порядке)
     * @param books массив книг
     * @return возвращает отсортированный по названию массив
     */
    private static ArrayList bookSorting(ArrayList<Book> books) {
        Book temp;

        for (int i = 0; i < books.size(); i++) {
            for (int j = 1 + i; j < books.size(); j++) {
                int compare = books.get(j).getBookTitle().compareTo(books.get(i).getBookTitle());
                if (compare < 0) {
                    temp = books.get(i);
                    books.set(i, books.get(j));
                    books.set(j, temp);
                }
/**
 * если находятся 2  или более книг с одинаковым названиям, то эти книги сортируются по году выпуска
 */
                if (compare == 0) {
                    if (books.get(j).getPublishingYear() < books.get(i).getPublishingYear()) {
                        temp = books.get(i);
                        books.set(i, books.get(j));
                        books.set(j, temp);
                    }
                }
            }
        }
        return books;
    }

    /**
     * метод  фильтрует книги по названию(пользователь вводит название книги, а метод ищет в массиве книги с этим названием)
     * @param books массив книг
     * @param names массив , в который заносятся книги с названием, которое мы ищем
     * @param nameBook название книги, которые мы ввели
     * @return возращает новый массив с параметрами книг(и), название которой мы искали
     */
    private static ArrayList<Book> checksName(ArrayList<Book> books, ArrayList<Book> names, String nameBook) {

        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).getBookTitle().equals(nameBook)) {
                names.add(books.get(i));
            }

        }
        return names;
    }

    /**
     * метод  фильтрует книги по автору(пользователь вводит автора, а метод ищет в массиве книги с этим автором)
     * @param books массив книг
     * @param persons новый массив , в который заносятся книги с автором, которого мы ищем
     * @param surname фамилия автора, которую нужно найти
     * @return возращает новый массив с параметрами книг(и), автора которой мы искали
     */
    private static ArrayList<Book> SearchName(ArrayList<Book> books, ArrayList<Book> persons, String surname) {
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).getAuthor().equals(surname))
                persons.add(books.get(i));
        }
        return persons;
    }

    /**
     * метод ищет среди всех книг книгу(и), жанр которых мы ввели
     * @param books массив книг
     * @param genreBook новый массив , в который заносятся книги с жанром, который мы ищем
     * @param genr введенный жанр
     * @return возращает новый массив с параметрами книг(и), жанр которой мы искали
     */

    private static ArrayList filtrationGenre(ArrayList<Book> books, ArrayList<Book> genreBook, String genr) {
        for (int i = 0; i < books.size(); i++) {
            if (genr.equals("комедия")) {
                if (books.get(i).getGenre() == Genre.COMEDY) {
                    genreBook.add(books.get(i));
                }
            }
            if (genr.equals("рассказ")) {
                if (books.get(i).getGenre() == Genre.TALE) {
                    genreBook.add(books.get(i));
                }
            }
            if (genr.equals("драма")) {
                if (books.get(i).getGenre() == Genre.DRAMA) {
                    genreBook.add(books.get(i));
                }
            }
            if (genr.equals("научная литература")) {
                if (books.get(i).getGenre() == Genre.SCIENTIFIC) {
                    genreBook.add(books.get(i));
                }
            }
            if (genr.equals("роман")) {
                if (books.get(i).getGenre() == Genre.NOVEL) {
                    genreBook.add(books.get(i));
                }
            }
        }

        return genreBook;
    }

    private static void isEmpty(ArrayList<Book> arrayList, String message) {
        if (arrayList.isEmpty()) {
            System.out.println(message + "\n");
        } else {
            print(arrayList);
        }
    }

    /**
     * метод, который позволяет каждый новый объект в arrayList выводить с новой строки
     * @param books массив книг
     */
    private static void print(ArrayList<Book> books) {
        for (int i = 0; i < books.size(); i++) {
            System.out.println(books.get(i));
        }
        System.out.println();
    }
}
